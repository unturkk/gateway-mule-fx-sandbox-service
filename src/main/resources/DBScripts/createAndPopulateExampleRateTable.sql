create table rate (
  buy_or_sell enum ('Buy', 'Sell'),
  base_currency_code varchar(3),
  base_currency_amount decimal(13,2),
  contra_currency_code varchar(3),
  contra_currency_amount decimal(13,2),
  receiving_method varchar(10),
  exchange_rate decimal(13,10),
  exchange_rate_precision int,
  exchange_rate_conversion_rule varchar(10)
);

insert into rate values
('Buy', 'EUR', 201, 'USD', 219.97, 'FXTrade', 1.0987, 5, 'Direct');

insert into rate values
('Sell', 'GBP', 100, 'USD', 121.96, 'FXTrade', 1.21961, 5, 'Direct');
