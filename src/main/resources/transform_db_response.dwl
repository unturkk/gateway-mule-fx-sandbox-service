%dw 1.0
%output application/json
---
payload map ((payload01, indexOfPayload01) -> {
	buy_or_sell: payload01.buy_sell_indicator,
	base_currency_amount: payload01.base_currency_amount as :number,
	base_currency_code: payload01.base_currency_code,
	contra_currency_amount: payload01.contra_currency_amount as :number,
	contra_currency_code: payload01.contra_currency_code,
	exchange_rate: payload01.exchange_rate as :number,
	exchange_rate_conversion_rule: payload01.exchange_rate_conversion_rule,
	exchange_rate_precision: payload01.exchange_rate_precision,
	receiving_method: payload01.receiving_method
})