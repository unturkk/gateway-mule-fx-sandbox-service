# README #

### Foreign Exchange API Mulesoft Project (Sandbox) ###

For now, only GET method on /rates is working and returning dummy data. There are no immediate plans to add more resources.

There are two sample requests and two sample responses. If the client request matches one of the two sample requests, corresponding sample response (fetched from DB) is returned.
If the client request has no errors, but it does not match any of the sample requests, a message of "Request doesn't match the example request" is returned.

If there are errors with the client request, the Mule exception object is passed to common error handling framework, so that it can be handled properly.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Kemal Unturk