%dw 1.0
%output application/json
---
{
	request-headers: inboundProperties,
	query-params: {
		(dummy_array: inboundProperties.'http.query.params'.dummy_array)
				when inboundProperties.'http.query.params'.dummy_array?,
		(dummy_multiple: inboundProperties.'http.query.params'.dummy_multiple as :number)
				when inboundProperties.'http.query.params'.dummy_multiple?,
		(buy_or_sell: inboundProperties.'http.query.params'.buy_or_sell)
				when inboundProperties.'http.query.params'.buy_or_sell?,
		(base_currency_code: inboundProperties.'http.query.params'.base_currency_code)
				when inboundProperties.'http.query.params'.base_currency_code?,
		base_currency_amount: inboundProperties.'http.query.params'.base_currency_amount as :number
				when inboundProperties.'http.query.params'.base_currency_amount
						matches /^[+-]?[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{2})?$/
				otherwise
			inboundProperties.'http.query.params'.base_currency_amount
				when inboundProperties.'http.query.params'.base_currency_amount?
				otherwise
					null,
		(contra_currency_code: inboundProperties.'http.query.params'.contra_currency_code)
				when inboundProperties.'http.query.params'.contra_currency_code?,
		(contra_currency_amount: inboundProperties.'http.query.params'.contra_currency_amount as :number)
				when inboundProperties.'http.query.params'.contra_currency_amount?,
		(receiving_method: inboundProperties.'http.query.params'.receiving_method)
				when inboundProperties.'http.query.params'.receiving_method?
	}
}